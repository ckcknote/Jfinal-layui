# Jfinal-layui-pro 专业版

#### 介绍
JFinal+layui极速开发企业应用管理系统，是以JFinal+layui为核心的企业应用项目架构，利用JFinal的特性与layui完美结合，达到快速启动项目的目的。让开发更简单高效，即使你不会layui，也能轻松掌握使用。该项目的核心功能有：登录、功能管理、角色管理（包含了权限管理）、用户管理、部门管理、系统日志、业务字典，报表管理、代码生成器、通用的附件上传、下载、导入、导出，echart图表统计，缓存，druid的sql监控，基本满足企业应用管理系统的需求，简化了前段代码，后台公用接口都封装完善，你只需要开发业务功能即可。从后端架构到前端开发，从开发到部署，这真正的展现了jfinal极速开发的魅力。

 

### pro和master版本的区别主要是前端界面的不同，在原有的基础上修改css，调整首页布局，打造一款美观、好用、专业的后台管理系统


#### 软件架构
软件架构说明：
核心架构：[jfinal](http://www.jfinal.com)，[jfinal-undertow](http://www.jfinal.com/doc/1-4)，[layui](https://www.layui.com/)，mysql，ehcach,rsa加密算法
系统权限:通过“用户-角色-功能”三者关系来实现系统的权限控制，操作简单明了，代码实现极其简单，完全可以替代shiro，你不用再去折腾shiro那一套了，这都是得益于jfinal架构的巧妙设计。
前端页面：封装了layui常用模块代码，参照使用例子，就能快速上手，无需担心不懂layui。
系统日志：操作日志、数据日志、登录日志，无需注解和手动添加，就能跟踪记录数据


#### 友情链接

 **JFinal-layui-pro专业版在线演示系统：** [JFinal极速开发企业应用管理系统](https://demo.qinhaisenlin.com/)

 **JFinal-layui交流群：970045838**

 **[视频教程-从入门到精通](https://www.qinhaisenlin.com/video   )**


#### 系统界面
1、登录界面，第一次不显示验证码，输错一次密码，则需要验证码，初始账号密码：admin/123456
![第一次登录界面](https://images.gitee.com/uploads/images/2020/1217/174600_440635b1_1692092.png "登录登录.png")

![密码错误，显示验证码](https://images.gitee.com/uploads/images/2020/1217/174703_205ef05a_1692092.png "显示验证码.png")
2、登录后的管理主页
![管理主页](https://images.gitee.com/uploads/images/2020/1222/205352_9487c450_1692092.png "系统管理主页.png")
3、系统管理核心模块
![功能管理](https://images.gitee.com/uploads/images/2020/1222/205555_32261dfa_1692092.png "功能管理.png")
![角色管理](https://images.gitee.com/uploads/images/2020/1222/205651_fe52e54d_1692092.png "角色管理.png")
![用户管理](https://images.gitee.com/uploads/images/2020/1222/205810_3dbc44b1_1692092.png "用户管理.png")
![部门管理](https://images.gitee.com/uploads/images/2020/1222/210053_ace40f91_1692092.png "部门管理.png")
![业务字典](https://images.gitee.com/uploads/images/2020/1222/210310_7dac4ab5_1692092.png "业务字典.png")
![系统日志](https://images.gitee.com/uploads/images/2020/1222/210349_446b56e5_1692092.png "系统日志.png")
![自定义SQL](https://images.gitee.com/uploads/images/2020/1222/210432_96eaa6a8_1692092.png "自定义sql.png")
![附件上传](https://images.gitee.com/uploads/images/2020/1217/160018_b02f38be_1692092.png "附件上传.png")
![附件列表](https://images.gitee.com/uploads/images/2020/1217/160059_447aacad_1692092.png "附件列表.png")
![echart图表](https://images.gitee.com/uploads/images/2020/1217/160140_a64d5839_1692092.png "echart图表.png")
![单表代码生成器](https://images.gitee.com/uploads/images/2020/1222/210914_44c91768_1692092.png "单表代码生成器.png")
![主从表代码生成器](https://images.gitee.com/uploads/images/2020/1217/160317_55b40123_1692092.png "主从表代码生成器.png")
![主从表示例](https://images.gitee.com/uploads/images/2020/1217/160810_05683f52_1692092.png "主从表示例.png")
![报表设计器](https://images.gitee.com/uploads/images/2020/1217/160913_3f056759_1692092.png "报表设计器.png")
![报表预览](https://images.gitee.com/uploads/images/2020/1217/160958_f6997107_1692092.png "报表预览.png")
![可编辑表格](https://images.gitee.com/uploads/images/2020/1217/161043_2b869643_1692092.png "可编辑表格.png")
![联级多选](https://images.gitee.com/uploads/images/2020/1217/161123_9f1cb41e_1692092.png "联级多选.png")
 **4、响应式布局展示：** 

![移动端主菜单](https://images.gitee.com/uploads/images/2020/1222/211404_034d7e13_1692092.png "移动端菜单.png")
![功能管理菜单树](https://images.gitee.com/uploads/images/2020/1222/211445_16e2dfd0_1692092.png "功能管理菜单树.png")
![功能管理列表](https://images.gitee.com/uploads/images/2020/1222/211616_9bf6a1e5_1692092.png "功能管理列表.png")
![功能管理添加弹窗](https://images.gitee.com/uploads/images/2020/1222/211652_f0cb6d53_1692092.png "功能管理添加弹窗.png")

![用户管理列表](https://images.gitee.com/uploads/images/2020/1222/211941_93cff64b_1692092.png "用户管理列表.png")
![删除提示](https://images.gitee.com/uploads/images/2020/1222/212333_7f747c6f_1692092.png "删除提示.png")

感兴趣的攻城狮可以参考，希望能对你有帮助。